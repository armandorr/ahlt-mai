# AHLT laboratories

Advanced Human Languages Technologies (AHLT) - Master in Artificial Intelligence

- Lab 1: Name Entity Recognition Classification
- Lab 2: Drug-Drug Interaction
- Lab 3: Neural Networks for NERC & DDI

## Authors
- Benjami Parellada
- Armando Rodriguez